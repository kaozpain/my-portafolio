<footer class="pt-2 pb-1 page-footer font-weight-bold blue fixed-bottom card" id="footer">

    <div class="col-12 float-left font-weight-bold text-center d-none d-lg-block">
        <information-item route="{{ route('home') }}" :translate-keys="{{ $vueText }}"/>
    </div>
    <!-- Copyright -->
    <div class="footer-copyright text-left py-2">
        <div class="d-flex ml-5 flex-wrap">
            <a class="flex-row-3 row pl-5"><strong>{{ trans('myapp.Language') }}:</strong>
                <form action="{{ url('functions/changeLanguage/') }}"  method="post">
                    @csrf
                    <select name="abbr" onchange="this.form.submit()">
                        @foreach($languages as $language)
                            <option {{$language['selected']?'selected':''}} value="{{strtolower($language['abbr'])}}">{{strtoupper($language['abbr']).'-'.$language['name']}}</option>
                        @endforeach
                    </select>
                </form>
            </a>
            <a class="flex-row-3 row pl-5"><strong>{{ trans('myapp.Client').' '.trans('myapp.Ip') }}:</strong>  {{ $clientIp?:trans('myapp.unknown') }}</a>
            <a class="flex-row-3 row pl-5"><strong>{{ trans('myapp.Client').' '.trans('myapp.City') }}:</strong>  {{ isset($clientInformation['city'])?$clientInformation['city']:trans('myapp.Unknown') }}</a>
            <a class="flex-row-3 row pl-5"><strong>{{ trans('myapp.Client').' '.trans('myapp.State') }}:</strong>  {{ isset($clientInformation['state'])?$clientInformation['state']:trans('myapp.Unknown') }}</a>
            <a class="flex-row-3 row pl-5"><strong>{{ trans('myapp.Client').' '.trans('myapp.Country') }}:</strong>  {{ isset($clientInformation['country'])?$clientInformation['country']:trans('myapp.Unknown') }}</a>
            <a class="flex-row-3 row pl-5"><strong>{{ trans('myapp.Client').' '.trans('myapp.Continent') }}:</strong>  {{ isset($clientInformation['continent'])?$clientInformation['continent']:trans('myapp.Unknown') }}</a>
        </div>
    </div>

</footer>