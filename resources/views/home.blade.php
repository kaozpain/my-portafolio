@extends('principal')
@section('content')
    <div class="position-relative p-3 p-md-12 m-md-3 text-center bg-light">
        <div class="col-md-10 p-lg-10 mx-auto my-5">
            <h1 class="display-3 font-weight-normal">{{ $my_name }}</h1>
            <p class="lead text-justify font-weight-normal">{{ $resume }}</p>
            <a class="btn btn-outline-success" href="{{ asset('/documents/CVAdrianMaldonadoBacigalupo.pdf') }}" download>{{ trans('myapp.Download') }} {{ trans('myapp.Resume') }}</a>
            <a class="btn btn-outline-danger" href="{{ asset('/documents/CLAdrianMaldonadoBacigalupo.pdf') }}" download>{{ trans('myapp.Download') }} {{ trans('myapp.Cover_Letter') }}</a>
        </div>
        <div class="product-device box-shadow d-none d-md-block"></div>
        <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
    </div>

    <div class="w-100 my-md-3 pl-md-3 pr-md-3">
        <div class="col-6 float-left">
        <div class="bg-dark  text-center text-white overflow-hidden ">
            <div class="my-3 py-3">
                <h2 class="display-5">{{ trans('myapp.Social_Media') }}</h2>
                @foreach($SocialMedias as $SocialMedia)
                    <a href="{{ $SocialMedia->link }}" class="float-none" target="_blank">
                        <icon-item name="{{ $SocialMedia->name }}" size="50"></icon-item>
                        <p>{{ $SocialMedia->name }}</p>
                    </a>
                @endforeach
            </div>
        </div>
        </div>
        <div class="col-6 float-right">
        <div class="bg-light  text-center overflow-hidden">
            <div class="my-3 p-3">
                <h2 class="display-5">{{ trans('myapp.Repositories') }}</h2>
                @foreach($Repositories as $Repository)
                    <a href="{{ $Repository->link }}" target="_blank">
                        <icon-item name="{{ $Repository->name }}" size="50"></icon-item>
                        <p>{{ $Repository->name }}</p>
                    </a>
                @endforeach
            </div>
        </div>
        </div>
    </div>

    <div class="w-100 my-md-3 pl-md-3 float-left" style="padding-bottom: 10%;">
        <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 p-3">
                <h2 class="display-5">{{ trans('myapp.Contact_Me') }}:</h2>
                <p class="lead">{{ trans('myapp.You_Can_Contact_Me_Via_Social_Media_Or_By_Filling_This_Form') }}:</p>
            </div>
            <div class="bg-dark box-shadow mx-auto overflow-auto container" style="border-radius: 21px 21px 0 0; overflow-x: hidden">
                <contact-form route="{{ route('home') }}" token="{{ csrf_token() }}" :Translated-text="{{ $tranlatedText }}"></contact-form>
            </div>
        </div>
    </div>
@endsection