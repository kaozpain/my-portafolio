@extends('principal')
@section('content')
    <div class="pt-5 pt-lg-0">
        <skill-items :skills-groups="{{ $skillsGroups }}"></skill-items>
    </div>
@endsection