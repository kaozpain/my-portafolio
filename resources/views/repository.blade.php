@extends('principal')
@section('content')
    @php($GoTo = trans('myapp.Go').' '.trans('myapp.To'))
    <div class="col-lg-10 mx-auto pb-5">
        <card-items :git-lab-data="{{ $gitData }}" Translated-text="{{ $GoTo }}"></card-items>
    </div>
@endsection