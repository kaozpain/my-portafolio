<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content text-justify">
        <div class="modal-header">
            <h5 class="modal-title">{{ trans('myapp.About_This_Page') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
                {{ $information }}
            </p>
        </div>
    </div>
</div>