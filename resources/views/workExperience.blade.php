@extends('principal')
@section('content')
    @php($goToWebPage = trans('myapp.Go') .' '.trans('myapp.To') .' '.trans('myapp.Web_Page'))
    <work-items :works="{{ $works }}" Translated-text="{{ $goToWebPage }}"></work-items>
@endsection