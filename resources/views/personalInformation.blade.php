@extends('principal')
@section('content')
    <div class="card align-items-center col-lg-7 col-10 mx-auto">
        <div class="card-header p-5 row-cols-2 text-center">
            <img class="card-img-top rounded-circle" src="{{ asset('/images/principal.png') }}" >
        </div>
        <div class="card-body">
            <dl class="row" style="margin-top: 5%">
                <dt class="col-sm-3">{{ trans('myapp.Full_Name') }}:</dt>
                <dd class="col-sm-9">{{ $PersonalInformation->name.' '.$PersonalInformation->lastname }}</dd>

                <dt class="col-sm-3">{{ trans('myapp.Nationality') }}:</dt>
                <dd class="col-sm-9">
                    @foreach($nationallities as $nationallity)
                        <p>{{ $nationallity['name'] }}</p>
                    @endforeach
                </dd>

                <dt class="col-sm-3">{{ trans('myapp.Degree') }}:</dt>
                <dd class="col-sm-9">{{ $PersonalInformation->getDegreeName() }}</dd>

                <dt class="col-sm-3">{{ trans('myapp.College') }}:</dt>
                <dd class="col-sm-9">{{ $study['name'] }} ({{ $study['start_date'] }} - {{ $study['finish_date'] }})</dd>

                <dt class="col-sm-3">{{ trans('myapp.Language') }}:</dt>
                <dd class="col-sm-9">
                    @foreach($Languages as $Language)
                    <p>{{ $Language['name'] }}</p>
                    @endforeach
                </dd>

                <dt class="col-sm-3">{{ trans('myapp.Civil_Status') }}:</dt>
                <dd class="col-sm-9">{{ $PersonalInformation->getCivilStatusName() }}</dd>
            </dl>
        </div>
    </div>
@endsection