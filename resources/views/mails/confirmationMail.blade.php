<h1>{{ trans('myapp.Hello') }} {{ $name }}</h1>
<br/>
<br/>
<h3>{{ trans('myapp.Thank_You_For_Contacting_Me') }},{{ trans('myapp.I_Will_Respond_You_As_Soon_As_Possible') }}.</h3>
<br>
<h3>{{ trans('myapp.Your_Message') }}: </h3>
<br>
<blockquote>{{ $clientMessage }}</blockquote>
<br>
<br>
<h1>{{ trans('myapp.Best_Regards') }}.</h1>
<h2>Adrian Maldonado</h2>