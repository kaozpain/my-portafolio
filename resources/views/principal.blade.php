<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Adrian Maldonado - {{ trans('myapp.Software_Engineer') }}</title>
        <link rel="icon" type="image/x-icon" href="{{ asset('images/icons/main.ico') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
        <style>
            body {
                background-image: url("{{ asset('/images/code.gif') }}")
            }
        </style>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="body">
        <div id="app">
            <div style="font-size: 20px">
                @include('layouts.topbar')
            </div>
            <div class="container-fluid p-5 col-lg-10" style="margin-block-end: 100px; font-size: 20px">
                @yield('content')
            </div>
            <div>
                @include('layouts.footer')
            </div>
        </div>
        <script src="{{ asset('/js/custom.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
    </body>
</html>