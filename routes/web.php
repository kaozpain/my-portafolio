<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    // ...
Route::get('/', 'HomeController@index')->name('home');

Route::prefix('pruebas')->group(function (){
    Route::get('/', function (){ return view('welcome'); });
});

Route::prefix('aboutMe')->group(function (){
    Route::get('/personalInformation', 'AboutMeController@personalInformationIndex')->name('personalInformation');
    Route::get('/workExperience', 'AboutMeController@workExperienceIndex')->name('workExperience');
});

Route::prefix('aboutThisPage')->group(function (){
    Route::get('/', 'HomeController@getModalAboutThisPage')->name('aboutThisPage');
});

Route::prefix('repositories')->group(function (){
    Route::get('/', 'RepositoryController@index')->name('repositories');
});

Route::prefix('skills')->group(function (){
    Route::get('/', 'SkillController@index')->name('skills');
});

Route::prefix('functions')->group(function (){
    Route::get('/deleteCache', 'Controller@deleteCache')->name('deleteCache');
    Route::post('/changeLanguage', function () {
        \Illuminate\Support\Facades\Session::put('language', $_POST['abbr']);
        return redirect()->back();
    })->name('language');
    Route::post('/contact', 'Controller@contact')->name('contact');
    Route::get('/getTranslation/{key}', 'Controller@getTraduction')->name('translate');
});
