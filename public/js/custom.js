let custom = {
    borrarCache: function () {
        $.ajax({
            url: "functions/deleteCache",
        }).done(function() {
            alert("Cache eliminada por completo");
        });
    },
    changeLanguage: function (abbr) {
        $.ajax({
            url: "functions/changeLanguage/"+abbr,
        }).done(function() {
            console.log("Changed succesfully");
        });
    },
};