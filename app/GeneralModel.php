<?php

namespace App;

use App\Models\Name;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class GeneralModel extends Model
{

    /**
     * Get data translated
     *
     * @return mixed
     */
    public function getName(){
        return $this->hasOne(Name::class, 'id', 'name')->pluck(App::getLocale())->first();
    }


    /**
     * sets data own magic methos
     *
     * @param $data
     */
    public function setData($data) {
        foreach ($data as $key => $item) {
            $this->$key = $item;
        }
    }

}
