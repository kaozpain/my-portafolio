<?php

namespace App\Providers;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Contact;
use Illuminate\Support\ServiceProvider;

class AppCallbacks extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Client::saving( function (Client $Model){
            $date = now();
            $date->modify('-5 minute');
            if($date > $Model->updated_at)
                $Model->count_views += 1;
        });
        Contact::saving( function (Contact $Model) {
            $ip = (new Controller())->getClientIp();
            $clientId = Client::where('ip', $ip)->first()->id;
            $Model->client_id = $clientId;
        });
    }
}
