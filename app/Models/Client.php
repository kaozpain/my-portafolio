<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 31/07/20
 * Time: 22:59
 */

namespace App\Models;


use App\GeneralModel;

/**
 * @property name
 *
 * Class Client
 * @package App\Models
 */
class Client extends GeneralModel
{
    protected $table = 'client';

    public function Contacts(){
        return $this->hasMany(Contact::class, 'client_id');
    }

}