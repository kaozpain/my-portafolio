<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 28/07/20
 * Time: 20:23
 */

namespace App\Models;


use App\GeneralModel;

class Study extends GeneralModel
{
    protected $table = 'study';
}