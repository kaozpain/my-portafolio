<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 26/07/20
 * Time: 20:27
 */

namespace App\Models;


use App\GeneralModel;

class Work extends GeneralModel
{

    protected $table = 'work';

    public function Responsabilities() {
//        var_dump($this->hasMany(Responsability::class, 'work_id')->toSql());
        return $this->hasMany(Responsability::class, 'work_id');
    }

    public function getInformationName(){
        $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == 'es'?'es':'en';
        return $this->hasOne(Name::class, 'id', 'information_name')->pluck($language)->first();
    }

    public function getPositionName(){
        $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == 'es'?'es':'en';
        return $this->hasOne(Name::class, 'id', 'position_name')->pluck($language)->first();
    }
}