<?php

namespace App\Models;

use App\GeneralModel;
use App\Models\Interfaces\RatingInterface;

class Skill extends GeneralModel implements RatingInterface
{

    protected $table = 'skill';

    public function Group(){
        return $this->hasOne(SkillGroup::class, 'group_id');
    }

    public function getRating()
    {
        return $this->rating;
    }
}
