<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 26/07/20
 * Time: 20:28
 */

namespace App\Models;


use App\GeneralModel;

class Responsability extends GeneralModel
{
    protected $table = 'responsability';
}