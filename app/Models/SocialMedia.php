<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 27/07/20
 * Time: 21:27
 */

namespace App\Models;


use App\GeneralModel;

class SocialMedia extends GeneralModel
{
    protected $table = 'social_media';
}