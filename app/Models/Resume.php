<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 28/07/20
 * Time: 20:55
 */

namespace App\Models;


use App\GeneralModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resume extends GeneralModel
{
    use SoftDeletes;

    protected $table = 'resume';
}