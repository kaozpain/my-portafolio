<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 28/07/20
 * Time: 20:22
 */

namespace App\Models;


use App\GeneralModel;
use App\Models\Interfaces\RatingInterface;

class Language extends GeneralModel implements RatingInterface
{

    protected $table = 'language';

    public function getRating()
    {
        return $this->rating;
    }
}