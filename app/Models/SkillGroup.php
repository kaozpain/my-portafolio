<?php

namespace App\Models;

use App\GeneralModel;

class SkillGroup extends GeneralModel
{
    protected $table = 'skill_group';
    public function Skills(){
        return $this->hasMany(Skill::class, 'skill_group_id');
    }
}