<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 31/07/20
 * Time: 22:59
 */

namespace App\Models;


use App\GeneralModel;

class Contact extends GeneralModel
{

    public const UPDATED_AT = null;

    protected $table = 'contact';


    public function Client(){
        return $this->belongsTo(Client::class, 'client_id');
    }
}