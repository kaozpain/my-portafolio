<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 27/07/20
 * Time: 21:32
 */

namespace App\Models;


use App\GeneralModel;

class PersonalInformation extends GeneralModel
{
    protected $table = 'personal_information';

    public function getDegreeName(){
        $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == 'es'?'es':'en';
        return $this->hasOne(Name::class, 'id', 'degree_name')->pluck($language)->first();
    }

    public function getCivilStatusName(){
        $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == 'es'?'es':'en';
        return $this->hasOne(Name::class, 'id', 'civil_status_name')->pluck($language)->first();
    }
}