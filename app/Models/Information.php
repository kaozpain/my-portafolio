<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 31/07/20
 * Time: 2:32
 */

namespace App\Models;


use App\GeneralModel;

class Information extends GeneralModel
{
    protected $table = 'information';
}