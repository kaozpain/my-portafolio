<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessApiToken extends Model
{

    protected $table = 'access_api_token';

    public static function isValidToken($token) {
        return !is_null(self::where('token', $token)->first());
    }
}
