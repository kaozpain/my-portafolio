<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 27/07/20
 * Time: 21:32
 */

namespace App\Models;


use App\GeneralModel;

class Repository extends GeneralModel
{
    protected $table = 'repository';
}