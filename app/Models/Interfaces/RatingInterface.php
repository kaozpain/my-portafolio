<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 30/07/20
 * Time: 11:36
 */

namespace App\Models\Interfaces;


interface RatingInterface
{

    public function getRating();
}