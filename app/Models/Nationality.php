<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 28/07/20
 * Time: 20:21
 */

namespace App\Models;


use App\GeneralModel;

class Nationality extends GeneralModel
{
    protected $table = 'nationality';

}