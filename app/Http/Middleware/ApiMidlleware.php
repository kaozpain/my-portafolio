<?php

namespace App\Http\Middleware;

use App\Models\AccessApiToken;
use Closure;

class ApiMidlleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access = AccessApiToken::isValidToken($request->get('token'));
        if ($access){
            return $next($request);
        } else {
            return response('Unauthorized.', 401);
        }
    }
}
