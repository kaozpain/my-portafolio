<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Closure;

class ControlAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $Controller = new Controller();
        $ip = $Controller->getClientIp();
        $data = $Controller->getClientIpInfo($ip);
        $Client = Client::where(['ip' => $ip])->first();
        if (is_null($Client)){
            $data['ip'] = $ip;
            unset($data['country']);
            unset($data['continent']);
            $Client = new Client();
            $Client->setData($data);
        }
        $Client->save();

        return $response;
    }
}
