<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 25/07/20
 * Time: 13:20
 */

namespace App\Http\Controllers;


use App\Models\PersonalInformation;
use App\Models\Repository;
use App\Models\Resume;
use App\Models\SocialMedia;
use phpDocumentor\Reflection\Types\Parent_;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * shows the home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $this->getLayoutData();
        $this->options['homePage']['selected'] = true;
        $data['opciones'] = json_encode($this->options);
        $data = array_merge($data, $this->footer);
        $data['name'] = 'home';
        $data['SocialMedias'] = SocialMedia::all();
        $data['Repositories'] = Repository::all();
        $PersonalInformation = PersonalInformation::all()->first();
        $data['my_name'] = explode(' ', $PersonalInformation->name)[0].' '.explode(' ', $PersonalInformation->lastname)[0];
        $data['resume'] = Resume::all()->first()->getName();
        $data['tranlatedText'] = json_encode([
            'confirmationBox' => trans('myapp.Send_Me_A_Confirmation_Mail'),
            'Name' => trans('myapp.Name'),
            'EMail' => trans('myapp.E_Mail'),
            'Message' => trans('myapp.Message'),
            'Send' => trans('myapp.Send'),
            'Sent_Success' => trans('myapp.Sent_Success'),
            'Sent_Fail' => trans('myapp.Sent_Fail'),
        ]);
        return view('home', $data);
    }

}