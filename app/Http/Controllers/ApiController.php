<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Contact;

class ApiController extends Controller
{
    public function getAllData() {
        $data = [];
        $Clients = Client::all();
        foreach ($Clients as $Client) {
            $data[$Client->id] = $Client->toArray();
            /** @var Contact $Contact */
            foreach ($Client->Contacts as $Contact) {
                $data[$Client->id]['contacts'][$Contact->id] = $Contact->toArray();
            }
        }

        return json_encode($data);
    }
}
