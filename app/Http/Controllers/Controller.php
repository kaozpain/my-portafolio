<?php

namespace App\Http\Controllers;

use App\Mail\Mailer;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Information;
use http\Env\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
    }

    public function getLayoutData() {
        $this->options = $this->__get_options();
        $this->footer = $this->footerData();
    }

    /**
     * sets default options
     *
     * @return array
     */
    public function __get_options(){
        $options = [
            'homePage' =>
                [
                    'name' => trans('myapp.Home'),
                    'tooltip' => trans('myapp.Home'),
                    'color' => 'red',
                    'selected_color' => 'blue',
                    'selected' => false,
                    'link' => URL::route('home'),
                    'collapsable' => []
                ],
            'repositoryPage' =>
                [
                    'name' => trans('myapp.Repositories'),
                    'tooltip' => trans('myapp.Repositories'),
                    'color' => 'red',
                    'selected_color' => 'blue',
                    'selected' => false,
                    'link' => URL::route('repositories'),
                    'collapsable' => []
                ],
            'skillPage' =>
                [
                    'name' => trans('myapp.Skills'),
                    'tooltip' => trans('myapp.Skills'),
                    'color' => 'red',
                    'selected_color' => 'blue',
                    'selected' => false,
                    'link' => URL::route('skills'),
                    'collapsable' => []
                ],
            'aboutMePage' =>
                [
                    'name' => trans('myapp.About_Me'),
                    'tooltip' => trans('myapp.About_Me'),
                    'color' => 'red',
                    'selected_color' => 'blue',
                    'selected' => false,
                    'link' => '#',
                    'collapsable' => [
                        [
                            'name' => trans('myapp.Personal_Information'),
                            'tooltip' => trans('myapp.Personal_Information'),
                            'link' => URL::route('personalInformation'),
                        ],
                        [
                            'name' => trans('myapp.Work_Experience'),
                            'tooltip' => trans('myapp.Work_Experience'),
                            'link' => URL::route('workExperience'),
                        ]
                    ]
                ],
        ];
        return $options;
    }

    /**
     * @deprecated
     *
     * Deletes session data
     */
    private function deleteCache() {
        Session::flush();
    }

    /**
     * Gets the Traduction by key
     *
     * @param $key
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTraduction ($key) {
        return trans('myapp.'.$key);
    }

    /**
     * get all supported Language
     *
     * @return array
     */
    public function getAllSupportedLanguages() {
        $supportedLanguage = [
            [
                'abbr' => 'Es',
                'name' => trans('myapp.Es')
            ],
            [
                'abbr' => 'En',
                'name' => trans('myapp.En')
            ],
        ];
        return $supportedLanguage;
    }

    /**
     * Get actual Language
     *
     * @return array
     */
    public function getActualLanguage() {
        $language = ucfirst(App::getLocale());
        return [
            'abbr' => $language,
            'name' => trans('myapp.'.$language)
        ];
    }


    /**
     * @TODO
     *
     * @param String $abbr
     */
    protected function changeLanguage(String $abbr) {
        $abbr = strtolower($abbr);
        if ($abbr != 'AUTOMATIC') {
            Session::put('_language', $abbr);
            Session::save();
        }
        var_dump(Session::all());
    }

    /**
     * default data for footer
     *
     * @return mixed
     */
    public function footerData() {
        $data['languages'] = $this->getLanguageOptions();
        $data['vueText'] = json_encode(['Information_About_This_Page']);
        $data['currentLanguage'] = $this->getActualLanguage();
        $ipAddress = $this->getClientIp();
        $data['clientIp'] = $ipAddress;
        $data['clientInformation'] = $this->getClientIpInfo($ipAddress);
        return $data;
    }


    /**
     * return a view for the about this page modal
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getModalAboutThisPage() {
        $data['information'] = Information::all()->first()->getName();
        return view('aboutThisPage', $data);
    }

    /**@TODO
     *
     * @return array
     */
    public function getLanguageOptions() {
        $languages = $this->getAllSupportedLanguages();
        $actualLanguageAbbr = $this->getActualLanguage()['abbr'];
        foreach ($languages as $key => $language) {
            $result[$key] = $language;
            $result[$key]['selected'] = ($language['abbr']==$actualLanguageAbbr);
        }
        return $result?:[];
    }

    /**
     * gets client Ip
     *
     * @return array|false|string
     */
    function getClientIp() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
     * Consumes an rest api for getting clients information by IP
     *
     * @param null $ip
     * @param string $purpose
     * @param bool $deep_detect
     * @return array|null|string
     */
    function getClientIpInfo($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => $ipdat->geoplugin_city,
                            "state"          => $ipdat->geoplugin_regionName,
                            "country"        => $ipdat->geoplugin_countryName,
                            "country_code"   => $ipdat->geoplugin_countryCode,
                            "continent"      => $this->getTraduction(strtoupper($ipdat->geoplugin_continentCode)),
                            "continent_code" => $ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = $ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = $ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = $ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = $ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = $ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    public function contact() {
        $postData = $_POST;
        $this->saveContact($postData);
        $this->to = $postData['confirmationMail']?$postData['mail']:"adrian272566@hotmail.es";
        $this->data = $postData;
        return $this->sendMail();
    }

    public function sendMail() {
        $mailer = new Mailer($this->data);
        return Mail::to($this->to)->send($mailer);
    }

    private function saveContact($data) {
        $Contact = new Contact();
        $data['message'] = $data['clientMessage'];
        $data['confirmation_mail'] = $data['confirmationMail']?1:0;
        unset($data['clientMessage'], $data['confirmationMail']);
        $Contact->setData($data);
        $Contact->save();
    }
}
