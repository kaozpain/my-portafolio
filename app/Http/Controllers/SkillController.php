<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 25/07/20
 * Time: 16:24
 */

namespace App\Http\Controllers;


use App\Models\Skill;
use App\Models\SkillGroup;

class SkillController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * shows the home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $this->getLayoutData();
        $this->options['skillPage']['selected'] = true;
        $data['opciones'] = json_encode($this->options);
        $data = array_merge($data, $this->footer);
        $data['name'] = 'home';
        $SkillsGroups = SkillGroup::all();
        foreach ($SkillsGroups as $SkillGroup){
            $data['skillsGroups'][$SkillGroup->id] = [
                'id' => $SkillGroup->id,
                'name' => $SkillGroup->getName()
            ];
            /** @var Skill $Skill */
            foreach ($SkillGroup->Skills as $Skill){
                $data['skillsGroups'][$SkillGroup->id]['Skills'][$Skill->id] = [
                    'id' => $Skill->id,
                    'name' => $Skill->getName(),
                    'rating' => $Skill->rating
                ];
            }
        }
        $data['skillsGroups'] = json_encode($data['skillsGroups']);

        return view('skill', $data);
    }
}