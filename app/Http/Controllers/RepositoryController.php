<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 25/07/20
 * Time: 16:24
 */

namespace App\Http\Controllers;


use GuzzleHttp\Client;

class RepositoryController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * shows the repository page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $this->getLayoutData();
        $this->options['repositoryPage']['selected'] = true;
        $data['opciones'] = json_encode($this->options);
        $data = array_merge($data, $this->footer);
        $data['name'] = 'home';
        $gitLabApiData = $this->consumeGitLabRestApi();
        $data['gitData'] = json_encode($this->getImportantDataGitLabApi(json_decode($gitLabApiData)));
        return view('repository', $data);
    }

    /**
     * gets the important data from the api response
     *
     * @param $gitLabApiData
     * @return array
     */
    public function getImportantDataGitLabApi($gitLabApiData){
        $showData = [];
        foreach ($gitLabApiData as $gitData){
            $showData[$gitData->id] = [
                'description' => $gitData->description?: trans('myapp.The_Description_Is_Empty').'. '.strtolower(trans('myapp.For_More_Information_Click_The_Link_Below')).'.',
                'name' => $gitData->name,
                'link' => $gitData->web_url,
            ];
        }
        return $showData;
    }

    /**
     * get the return value of the rest api
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function consumeGitLabRestApi(){
        $Client = new Client();
        $result = $Client->get('https://gitlab.com/api/v4/users/2820558/projects');
        if ($result->getStatusCode() == '200')
        {
            return $result->getBody();
        }
    }
}