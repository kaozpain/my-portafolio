<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 25/07/20
 * Time: 16:24
 */

namespace App\Http\Controllers;


use App\Models\Language;
use App\Models\Nationality;
use App\Models\PersonalInformation;
use App\Models\Responsability;
use App\Models\Study;
use App\Models\Work;

class AboutMeController extends Controller
{

    const CURRENT = 'Working';
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * shows the home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function personalInformationIndex(){
        $this->getLayoutData();
        $this->options['aboutMePage']['selected'] = true;
        $data['opciones'] = json_encode($this->options);
        $data = array_merge($data, $this->footer);
        $data['name'] = 'home';
        $data['PersonalInformation'] = PersonalInformation::all()->first();
        $Nationallities = Nationality::all();
        $data['nationallities'] = $Nationallities->map(function ($Nationallity) {
            return [
                'id' => $Nationallity->id,
                'name' => $Nationallity->getName(),
            ];
        });
        $data['Languages'] = (Language::all())->map(function ($Language) {
            return [
                'id' => $Language->id,
                'name' => $Language->getName(),
                'rating' => $Language->rating,
            ];
        });
        $data['study'] = array_map(function ($array){
            return [
                'id' => $array['id'],
                'name' => $array['name'],
                'start_date' => date('m/Y',strtotime($array['start_date'])),
                'finish_date' => !is_null($array['finish_date'])?date('m/Y',strtotime($array['finish_date'])):self::CURRENT,
            ];
        },Study::all()->toArray())[0];

        return view('personalInformation', $data);
    }

    /**
     * shows the home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function workExperienceIndex(){
        $this->getLayoutData();
        $this->options['aboutMePage']['selected'] = true;
        $data['opciones'] = json_encode($this->options);
        $data = array_merge($data, $this->footer);
        $data['name'] = 'home';
        $Works = Work::all();
        foreach ($Works as $work){
            $data['work'][$work->id] = [
                'id' => $work->id,
                'companyName' => $work->company,
                'information' => $work->getInformationName(),
                'position' => $work->getPositionName(),
                'link' => $work->link,
                'start_date' => date('m/y',strtotime($work->start_date)),
                'finish_date' => !is_null($work->finish_date)?date('m/y',strtotime($work->finish_date)):self::CURRENT,
            ];
            /** @var Responsability $responsability */
            foreach ($work->Responsabilities as $responsability){
                $data['work'][$work->id]['responsabilities'][$responsability->id] = [
                    'id' => $responsability->id,
                    'name' => $responsability->getName()
                ];
            }
        }
        $data['works'] = json_encode($data['work']);
        return view('workExperience', $data);
    }
}