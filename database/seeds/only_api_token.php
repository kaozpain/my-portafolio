<?php

use Illuminate\Database\Seeder;

class only_api_token extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOnlyAccesToken();
    }

    public function createOnlyAccesToken() {
        $token = \Illuminate\Support\Str::random(60);
        \Illuminate\Support\Facades\DB::table('access_api_token')->insert([
            'token' => hash('sha256', $token)
        ]);
    }
}
